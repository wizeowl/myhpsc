program main
    use omp_lib
    implicit none
    integer, parameter :: logn_max = 10
    write ( *, '(a)') ' '
    write ( *, '(a)') 'COMPUTE_PI'
    write ( *, '(a)') '  FORTRAN90/OpenMP version'
    write ( *, '(a)') ' '
    write ( *, '(a)') '  Estimate the value of PI by summing a series.'

    write ( *, '(a)') ' '
    write ( *, '(a,i8)') '  The number of processors available = ', omp_get_num_procs()
    write ( *, '(a,i8)') '  The number of threads available    = ', omp_get_max_threads()
    call r8_test(logn_max)
    !
    !  Terminate.
    !
    write ( *, '(a)') ' '
    write ( *, '(a)') 'COMPUTE_PI'
    write ( *, '(a)') '  Normal end of execution.'
    stop
end program main

subroutine r8_test(logn_max)
    use omp_lib
    implicit none
    double precision error
    double precision estimate
    integer logn
    integer logn_max
    character(len = 3) mode
    integer n
    double precision, parameter :: r8_pi = 3.141592653589793D+00
    double precision wtime

    write ( *, '(a)') ' '
    write ( *, '(a)') 'R8_TEST:'
    write ( *, '(a)') '  Estimate the value of PI,'
    write ( *, '(a)') '  using double precision arithmetic.'
    write ( *, '(a)') ' '
    write ( *, '(a)') '  N = number of terms computed and added;'
    write ( *, '(a)') ' '
    write ( *, '(a)') '  MODE = SEQ for sequential code;'
    write ( *, '(a)') '  MODE = OMP for Open MP enabled code;'
    write ( *, '(a)') '  (performance depends on whether Open MP is used,'
    write ( *, '(a)') '  and how many processes are available!)'
    write ( *, '(a)') ' '
    write ( *, '(a)') '  ESTIMATE = the computed estimate of PI;'
    write ( *, '(a)') ' '
    write ( *, '(a)') '  ERROR = ( the computed estimate - PI );'
    write ( *, '(a)') ' '
    write ( *, '(a)') '  TIME = elapsed wall clock time;'
    write ( *, '(a)') ' '
    write ( *, '(a)') '  Note that you can''t increase N forever, because:'
    write ( *, '(a)') '  A) ROUNDOFF starts to be a problem, and'
    write ( *, '(a)') '  B) maximum integer size is a problem.'
    write ( *, '(a)') ' '
    write ( *, '(a,i24)') '  The maximum integer:', huge(n)
    write ( *, '(a)') ' '
    write ( *, '(a)') ' '
    write ( *, '(a)') '             N Mode    Estimate        Error           Time'
    write ( *, '(a)') ' '

    n = 1
    do logn = 1, logn_max
        ! 
        !  Sequential calculation.
        !
        mode = 'SEQ'
        wtime = omp_get_wtime()
        call r8_pi_est_seq(n, estimate)
        wtime = omp_get_wtime() - wtime
        error = abs(estimate - r8_pi)
        write ( *, '( i14, 2x, a3, 2x, f14.10, 2x, g14.6, 2x, g14.6 )') &
        n, mode, estimate, error, wtime
        !
        !  Open MP enabled calculation.
        !
        mode = 'OMP'
        wtime = omp_get_wtime()
        call r8_pi_est_omp(n, estimate)
        wtime = omp_get_wtime() - wtime
        error = abs(estimate - r8_pi)
        write ( *, '( i14, 2x, a3, 2x, f14.10, 2x, g14.6, 2x, g14.6 )') &
        n, mode, estimate, error, wtime
        n = n * 10
    end do
    return
end subroutine r8_test

subroutine r8_pi_est_omp(n, estimate)
    implicit none
    double precision h
    double precision estimate
    integer i
    integer n
    double precision sum2
    double precision x

    h = 1.d0 / dble(2 * n)
    sum2 =  0.d0
    !$omp parallel shared(h, n) private (i, x)
    !$omp do reduction(+ : sum2)
    do i = 1, n
        x = h * dble(2 * i - 1)
        sum2 = sum2 + 1.d0 / (1.d0 + x ** 2)
    end do
    !$omp end do
    !$omp end parallel
    estimate = 2.d0 * sum2 / dble(n)
    return
end subroutine r8_pi_est_omp

subroutine r8_pi_est_seq(n, estimate)
    implicit none
    double precision h
    double precision estimate
    integer i
    integer n
    double precision sum2
    double precision x
    h = 1.d0 / dble(2 * n)
    sum2 = 0.d0
    do i = 1, n
        x = h * dble(2 * i - 1)
        sum2 = sum2 + 1.d0 / (1.d0 + x**2)
    end do
    estimate = 4.d0 * sum2 / dble(n)
    return
end subroutine r8_pi_est_seq