program main
    use omp_lib
    implicit none
    integer(kind = 4) n
    integer(kind = 4) seed
    write ( *, '(a)') ' '
    write ( *, '(a)') 'RANDOM_OPENMP'
    write ( *, '(a)') '  FORTRAN90 version'
    write ( *, '(a)') '  An OpenMP program using random numbers.'
    write ( *, '(a)') '  The random numbers depend on a seed.'
    write ( *, '(a)') '  We need to insure that each OpenMP thread'
    write ( *, '(a)') '  starts with a different seed.'
    write ( *, '(a)') ' '
    write ( *, '(a,i8)') &
    '  Number of processors available = ', omp_get_num_procs()
    write ( *, '(a,i8)') &
    '  Number of threads =              ', omp_get_max_threads()
    n = 100
    seed = 123456789
    call monte_carlo(n, seed)
    write ( *, '(a)') ' '
    write ( *, '(a)') 'RANDOM_OPENMP'
    write ( *, '(a)') '  Normal end of execution.'

    write ( *, '(a)') ' '
    stop
end program main

subroutine monte_carlo(n, seed)
    use omp_lib
    implicit none
    integer(kind = 4) n
    integer(kind = 4) i
    integer(kind = 4) my_id
    integer(kind = 4) my_seed
    integer(kind = 4) seed
    real(kind = 8) x(n)

    !$omp master
    write ( *, '(a)') ' '
    write ( *, '(a)') '  Thread   Seed  I   X(I)'
    write ( *, '(a)') ' '
    !$omp end master

    !$omp parallel private(i, my_id, my_seed) shared(n, x)
    my_id = omp_get_thread_num()
    my_seed = seed + my_id
    write ( *, '(2x,i6,2x,i12)') my_id, my_seed
    !$omp do
    do i = 1, n
        call random_value(my_seed, x(i))
        write ( *, '(2x,i6,2x,i12,2x,i6,2x,g14.6)') my_id, my_seed, i, x(i)
    end do
    !$omp end do
    !$omp end parallel
    return
end subroutine monte_carlo

subroutine random_value(seed, r)
    implicit none
    real(kind = 8) r
    integer(kind = 4) seed
    seed = mod(seed, 65536)
    seed = mod((3125 * seed), 65536)
    r = real(seed, kind = 8) / 96636.0D+00
    return
end subroutine random_value