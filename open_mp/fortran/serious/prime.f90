program main
    use omp_lib
    implicit none
    integer(kind = 4) n_factor
    integer(kind = 4) n_hi
    integer(kind = 4) n_lo
    write ( *, '(a)') ' '
    write ( *, '(a)') 'PRIME_NUMBER_OPENMP'
    write ( *, '(a)') '  FORTRAN90/OpenMP version'

    write ( *, '(a)') ' '
    write ( *, '(a,i8)') &
    '  Number of processors available = ', omp_get_num_procs()
    write ( *, '(a,i8)') &
    '  Number of threads =              ', omp_get_max_threads()
    n_lo = 1
    n_hi = 131072
    n_factor = 2
    call prime_number_sweep_openmp(n_lo, n_hi, n_factor)
    n_lo = 1
    n_hi = 500000
    n_factor = 10
    call prime_number_sweep_openmp(n_lo, n_hi, n_factor)
    !
    !  Terminate.
    !
    write ( *, '(a)') ' '
    write ( *, '(a)') 'PRIME_NUMBER_OPENMP'
    write ( *, '(a)') '  Normal end of execution.'
    stop
end program main

subroutine prime_number_sweep_openmp(n_lo, n_hi, n_factor)
    use omp_lib
    implicit none
    integer(kind = 4) i
    integer(kind = 4) n
    integer(kind = 4) n_factor
    integer(kind = 4) n_hi
    integer(kind = 4) n_lo
    integer(kind = 4) primes
    real(kind = 8) wtime
    write ( *, '(a)') ' '
    write ( *, '(a)') 'PRIME_NUMBER_SWEEP_OPENMP'
    write ( *, '(a)') '  Call PRIME_NUMBER to count the primes from 1 to N.'
    write ( *, '(a)') ' '
    write ( *, '(a)') '         N        Pi          Time'
    write ( *, '(a)') ' '
    n = n_lo
    do while (n <= n_hi)
        wtime = omp_get_wtime()
        call prime_number(n, primes)
        wtime = omp_get_wtime() - wtime
        write ( *, '(2x,i8,2x,i8,g14.6)') n, primes, wtime
        n = n * n_factor
    end do
    return
end subroutine prime_number_sweep_openmp

subroutine prime_number(n, total)
    implicit none
    integer(kind = 4) i
    integer(kind = 4) j
    integer(kind = 4) n
    integer(kind = 4) prime
    integer(kind = 4) total
    total = 0
    !$omp parallel private(i, j, prime) shared(n)
    !$omp do reduction(+ : total)
    do i = 2, n
        prime = 1
        do j = 2, i - 1
            if (mod(i, j) == 0)then
                prime = 0
                exit
            end if
        end do
        total = total + prime
    end do
    !$omp end do
    !$omp end parallel
    return
end subroutine prime_number