subroutine subdomain(x, istart, ipoints)
    integer :: istart, ipoints, i
    real(kind = 8) :: x
    do 100 i = 1, points
        x(istart + i) = 123.456
        100 continue
    enddo
end subroutine subdomain

subroutine sub(x, npoints)
    use omp_lib
    real(kind = 8) :: x
    integer :: npoints, iam, nt, ipoints, istart
    !$OMP PARALLEL DEFAULT(PRIVATE) SHARED(x, npoints)
    iam = omp_get_thread_num()
    nt = omp_get_num_threads()
    ipoints = npoints / nt
    istart = iam * ipoints
    if (iam .eq. nt - 1)then
        ipoints = npoints - istart
    endif
    call subdomain(x, istart, ipoints)
    !$OMP END PARALLEL
end subroutine sub

program parexample
    real(kind = 8) :: set(10000)
    call sub(set, 10000)
end program parexample