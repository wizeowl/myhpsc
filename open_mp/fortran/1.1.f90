subroutine simple(n, a, b)
    implicit none
    integer :: i, n
    real(kind = 8) :: b(n), a(n)

    !$OMP PARALLEL DO
    do i = 2, n
        b(i) = (a(i) + a(i - 1)) / 2
    end do
    !$OMP END PARALLEL DO
end subroutine simple