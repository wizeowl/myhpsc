subroutine ploop_2(a, b, n, i1, i2)
    real(kind = 8) :: a(n), b(n)
    integer :: i1, i2, n
    !omp parallel shared(a, b, i1, i2)
    !omp sections
    !omp section
    do i1 = i1, n
        if (a(i1) .ne. 0.0)then
            exit
        end if
    end do
    !omp section
    do i2 = i2, n
        if (b(i2) .ne. 0.0)then
            exit
        end if
    end do
    !omp end sections
    !omp single
    if (i1 .le. n)then
        print *, "a.. not all zeros"
    end if
    if (i2 .le. n)then
        print *, "b.. not all zeros"
    end if
    !omp end single
    !omp end parallel
end subroutine ploop_2