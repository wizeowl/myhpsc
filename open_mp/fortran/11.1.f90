subroutine sub(a)
    real(kind = 8) :: a(*)
    integer :: kl, ku, ks, jl, ju, js, il, iu, is
    integer :: i, j, k
    !omp do collapse(2) private(i, j, k)
    do k = kl, ku, ks
        do j = jl, ju, js
            do i = il, iu, is
                call bar(a, i, j, k)
            end do
        end do
    end do
    !omp end do
end subroutine sub