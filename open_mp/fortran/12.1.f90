subroutine sect_example()
    !$omp parallel sections
    !$omp section
    call xaxis()
    !$omp section
    call yaxis()
    !$omp section
    call zaxis()
    !$omp end parallel sections
end subroutine sect_example