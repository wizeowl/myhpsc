subroutine nowait_exp(n, m, a, b, y, z)
    integer :: n, m
    real(kind = 8) :: a(*), b(*), x(*), z(*)
    integer :: i
    !$omp parallel
    !$omp do
    do i = 2, n
        b(i) = (a(i) + a(i - 1)) / 2.0
    end do
    !$omp end do nowait
    !$omp do
    do i = 1, n
        y(i) = sqrt(z(i))
    end do
    !$omp end do nowait

    !$omp end parallel
end subroutine nowait_exp