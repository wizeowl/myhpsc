subroutine ploop_1(a, n)
    use omp_lib
    real(kind = 8) :: a(n)
    integer :: i, n, myoffset
    !$omp parallel private(myoffset)
    myoffset = omp_get_thread_num() * n
    do i = 1, n
        a(myoffset + i) = float(i)
    end do
    !$omp end parallel
end subroutine ploop_1