program example
    include "omp_lib.h"
    integer :: datum, flag
    flag = 0
    !$OMP PARALLEL NUM_THREADS(2)
    if (omp_get_thread_num() .eq. 0)then
        datum = 42
        !$OMP FLUSH(flag, datum)
        flag = 1
        !$OMP FLUSH(flag)
    else if (omp_get_thread_num() .eq. 1) then
        !$OMP FLUSH(flag, datum)
        do while (flag .lt. 1)
            !$OMP FLUSH(flag, datum)
        end do
        print *, "flag = ", flag, " datum = ", datum
        !$OMP FLUSH(flag, datum)
        print *, "flag = ", flag, " data = ", datum
    endif
    !$OMP END PARALLEL
end program example