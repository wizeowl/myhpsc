program example
    include "omp_lib.h"
    integer :: flag
    flag = 0
    !$OMP PARALLEL NUM_THREADS(3)
    if (omp_get_thread_nu() .eq. 0) then
        !$OMP ATOMIC
        flag = flag + 1
    else if (omp_get_thread_nu() .eq. 1) then
        !$OMP FLUSH(flag)
        do while (flag .lt. 1)
            !$OMP FLUSH(flag)
        end do
        print *, "Thread one, woke up!!"
        !$OMP ATOMIC UPDATE
        flag = flag + 1
    else if (omp_get_thread_nu() .eq. 2) then
        !$OMP FLUSH(flag)
        do while (flag .lt. 2)
            !$OMP FLUSH(flag)
        end do
        print *, "Thread 2, what took you so long!!"
        print *, "Weeeh! I was flushing some flags"
    endif
    !$OMP END PARALLEL
end program example
