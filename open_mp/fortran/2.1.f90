program memmodel
    implicit none
    include "omp_lib.h"
    integer :: x
    x = 2
    !$OMP PARALLEL NUM_THREADS(2) SHARED(X)
    if (omp_get_thread_nu() .eq. 0) then
        x = 5
    else
        print *, "1: thread# ", omp_get_thread_num(), " x = ", x
    endif
    !$OMP BARRIER
    if (omp_get_thread_num() .eq. 0) then
        print *, "2: Thread# ", omp_get_thread_num(), " x = ", x
    else
        print *, "3: Thread# ", omp_get_thread_num(), " x = ", x
    endif
    !$OMP END PARALLEL
end program memmodel