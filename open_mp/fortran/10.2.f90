subroutine nowait_exp2(n, a, b, c, y, z)
    integer :: n, i
    real(kind = 8) :: a(*), b(*), c(*), y(*), z(*)
    !$omp parallel
    !$omp do schedule(static)
    do i = 1, n
        c(i) = (a(i) + b(i)) / 2.0
    end do
    !$omp end do nowait
    !$omp do schedule(static)
    do i = 1, n
        z(i) = sqrt(c(i))
    end do
    !$omp end do nowait
    !$omp do schedule(static)
    do i = 2, n + 1
        y(i) = z(i - 1) + a(i)
    end do
    !$omp end do nowait
    !$omp end parallel
end subroutine nowait_exp2