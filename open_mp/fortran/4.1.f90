program icv
    use omp_lib
    call omp_set_nested(.true.)
    call omp_set_max_active_levels(8)
    call omp_set_dynamic(.false.)
    call omp_set_num_threads(2)
    
    !$OMP PARALLEL
    call omp_set_num_threads(3)
    !$OMP PARALLEL
    call omp_set_num_threads(4)
    !$OMP SINGLE
    print *, "Inner: max_act_level = ", omp_get_max_active_levels(), &
    "num_thds = ", omp_get_num_threads(), &
    "max_thds = ", omp_get_max_threads()
    !$OMP END SINGLE
    !$OMP END PARALLEL
    !$OMP BARRIER
    !$OMP SINGLE
    print *, "Outer: max_act_level = ", omp_get_max_active_levels(), &
    "num_thds = ", omp_get_num_threads(), &
    "max_thds = ", omp_get_max_threads()
    !$OMP END SINGLE
    !$OMP END PARALLEL
end program icv
