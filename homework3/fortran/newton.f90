module newton
  implicit none
  integer, parameter:: maxiter = 30
  real(kind=8), parameter:: tol = 1.d-14
contains
  
  subroutine solve(f, fp, x0, x, iters, debug)
    implicit none
    real(kind=8), intent(in):: x0
    real(kind=8), external:: f, fp
    logical, intent(in):: debug
    real(kind=8), intent(out):: x
    integer, intent(out):: iters
    
    real(kind=8):: deltax, fx, fxprime
    integer:: k
    
    x = x0
    
    if(debug) then
      print 11, x
      11 format('initial guess: x= ', es22.15)
    endif
    
    do k=1, maxiter
      fx = f(x)
      fxprime = fp(x)
      if(abs(fx) < tol) then
        exit
      endif
      
      deltax = fx / fxprime
      
      x = x - deltax
      
      if(debug) then
        print 12, k, x
        12 format('After', i3, ' iterations, x = ', es22.15)
      endif
      
    enddo
    
    if(k>maxiter) then
      fx = f(x)
      if(abs(fx) > tol) then
        print *, '*** arning: has not yet converged ***'
      endif
    endif
    
    iters = k - 1
    
  end subroutine solve
  
end module newton